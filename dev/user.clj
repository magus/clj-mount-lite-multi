(ns user
  (:require [mount.lite :as mount]
            [mount.extensions.namespace-deps :as mnd]
            [ml-prof.comp.compa :as a]
            [ml-prof.comp.compb :as b]
            ))

(comment ;; everything here
  (mount/start)
  (mount/stop)
  )

(comment ;; only A
  (mnd/start #'a/comp-state)
  (mnd/stop)
  )

(comment ;; only B
  (mnd/start #'b/comp-state)
  (mnd/stop)
  )
