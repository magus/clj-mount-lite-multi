(ns ml-prof.comp.compa
  (:require [mount.lite :as mount]
            [java-time :as t]
            [ml-prof.comp.comp-base :as base]))

(mount/defstate comp-state
  :start (do (println "Starting CompA")
             {:compa "compa stuff"
              :time (t/instant)})
  :stop (println "Stopping CompA"))
