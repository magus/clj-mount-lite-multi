(ns ml-prof.comp.compb
  (:require [mount.lite :as mount]
            [java-time :as t]
            [ml-prof.comp.comp-base :as base]))

(mount/defstate comp-state
  :start (do (println "Starting CompB")
             {:compb "compb stuff"
              :time (t/instant)})
  :stop (println "Stopping CompB"))
