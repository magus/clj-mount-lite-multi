(ns ml-prof.comp.comp-base
  (:require [mount.lite :as mount]
            [java-time :as t]))

(mount/defstate comp-state
  :start (do (println "Starting CompBase")
             {:foo "foo"
              :time (t/instant)})
  :stop (println "Stopping CompBase"))
