(defproject mount-lite-multiprof "0.0.1-SNAPSHOT"
  :description "Experiment with multiple mount-lite profiles"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/tools.cli "1.0.194"]
                 [org.clojure/tools.namespace "1.0.0"]
                 [clojure.java-time "0.3.2"]

                 ;; integrant stuff
                 [functionalbytes/mount-lite "2.1.3"]]
  :main ^:skip-aot ml-prof.main
  :profiles {:dev {:source-paths ["dev"]}
             :repl {:repl-options {:init-ns user}}})
